# frozen_string_literal: true

# This is the base mailer from which all others descend.
class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'
end
