# frozen_string_literal: true

# == Schema Information
#
# Table name: comments
#
#  id         :integer          not null, primary key
#  parent_id  :integer
#  user_id    :integer
#  title      :string(255)
#  content    :text(65535)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  category   :integer          default("newspost"), not null
#  visible    :boolean          default(FALSE), not null
#
class Post < ApplicationRecord
  belongs_to :parent, class_name: 'Post', optional: true
  belongs_to :user
  has_one_attached :upload

  enum category: {
    newspost: 0, # Appears on the front page.
    article: 1, # Appears on the article index.
    comment: 2, # Must have a parent.
    download: 3 # Must have an attached file.
  }

  validates :title, presence: true
  validates :content, presence: true, unless: :download?
  validates :parent, absence: true, unless: :comment?
  validates :parent, presence: true, if: :comment?
  # See https://github.com/rails/rails/issues/31656
  validate :upload_exists, if: :download?
  validate :upload_absent, unless: :download?

  def upload_type
    return '' unless upload.attached?

    %w[image video audio].each do |kind|
      return kind if upload.blob.content_type.starts_with?("#{kind}/")
    end
    'general'
  end

  def to_s
    title
  end

  private

  def upload_absent
    return unless upload.attached?

    errors.add(:upload, 'must be blank')
  end

  def upload_exists
    return if upload.attached?

    errors.add(:upload, 'is missing')
  end
end
