# frozen_string_literal: true

# This is the base class from which all model descend.
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
