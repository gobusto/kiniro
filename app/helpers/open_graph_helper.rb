# frozen_string_literal: true

require 'redcarpet/render_strip' # https://github.com/vmg/redcarpet/issues/138

# Helper methods related OGP metadata - see http://ogp.me/
module OpenGraphHelper
  def ogp_tags(title, url, description = nil)
    values = {
      'og:site_name' => 'kiniro',
      'og:type' => 'website',
      'og:title' => title,
      'og:description' => description || "#{title} @ kiniro.uk",
      'og:url' => url,
      'og:image' => image_url('avatar.png')
    }

    safe_join(values.map { |k, v| tag(:meta, property: k, content: v) })
  end

  def ogp_tags_for(post)
    plaintext_converter = Redcarpet::Markdown.new(Redcarpet::Render::StripDown)
    ogp_tags(
      post.title,
      post.download? ? download_url(post) : article_url(post),
      plaintext_converter.render(post.content).truncate(99)
    )
  end
end
