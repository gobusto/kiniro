# frozen_string_literal: true

# Helper methods related to users.
module UserHelper
  def avatar(user)
    # src = url_for(user.avatar.variant(resize: '60x60')) if user&.avatar&.attached?
    src = url_for(user.avatar) if user&.avatar&.attached?

    src ||= 'avatar.png'
    image_tag(src, class: 'avatar')
  end
end
