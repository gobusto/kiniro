# frozen_string_literal: true

# General helper methods.
module ApplicationHelper
  TAGLINES = [
    'bol is bleeding',
    'But Flaid, how are you old?',
    'Evil pirates?',
    'This Wizraz is nice and cool',
    'Why is it that I am bald, Garfield?'
  ].freeze

  def random_tagline
    TAGLINES.sample
  end
end
