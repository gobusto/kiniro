# frozen_string_literal: true

# Helper methods related to comments.
module CommentHelper
  def visibility_text(comment)
    comment.visible ? 'Public' : 'Hidden'
  end
end
