# frozen_string_literal: true

# Helper methods for forms.
module FormHelper
  def field_with_label(form, name, &)
    content = safe_join([form.label(name), capture(&)])
    content_tag(:div, content, class: 'field-with-label')
  end
end
