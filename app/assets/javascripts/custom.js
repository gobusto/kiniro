(function () {
  "use strict";

  // On any page, automatically add syntax-highlighting to code blocks:
  function initSyntaxHighlighting() {
    var elements = document.querySelectorAll("pre code");
    Array.prototype.forEach.call(elements, function(code) {
      hljs.highlightBlock(code);
    });
  }
  
  // On the "Edit Post" page ONLY, update the live Markdown preview on changes:
  function initMarkdownPreview() {
    var content = document.getElementById('post_content');
    var preview = document.getElementById('post_preview');
    if (!content) { return; }

    var reader = new commonmark.Parser();
    var writer = new commonmark.HtmlRenderer();

    var update = function () {
      var result = reader.parse(content.value);
      preview.innerHTML = writer.render(result);
    };
    
    // Update after a short delay if no further change events are fired:
    var timeout = null;
    var queueUpdate = function () {
      clearTimeout(timeout);
      timeout = setTimeout(update, 500);
    }

    update();
    content.addEventListener('change', queueUpdate);
    content.addEventListener('keyup', queueUpdate);

    // If the user presses tab, insert four spaces:
    content.addEventListener('keypress', function (e) {
      if (e.key != 'Tab' || e.shiftKey) { return; }
      console.log('TODO: Insert four spaces / indent selected text...');
      e.preventDefault();
    });
  }

  window.addEventListener("turbolinks:load", function () {
    initSyntaxHighlighting();
    initMarkdownPreview();
  });
})();
