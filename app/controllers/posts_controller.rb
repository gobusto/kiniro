# frozen_string_literal: true

# This handles the editing of posts; most visitors won't see it.
class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action -> { @post = Post.new }, only: %i[new create]
  before_action -> { @post = Post.find(params[:id]) }, except: %i[new create]

  def new
    if params[:parent].present?
      @post.parent_id = params[:parent]
      @post.category = 'comment'
    elsif %w[newspost article download].include?(params[:category])
      @post.category = params[:category]
    end
    render :edit
  end

  def create
    @post.user = current_user
    if @post.update(post_params)
      redirect_to suitable_page_for(@post)
    else
      render :edit
    end
  end

  def edit; end

  def update
    if @post.update(post_params)
      redirect_to suitable_page_for(@post)
    else
      render :edit
    end
  end

  def destroy
    if @post.destroy
      redirect_to suitable_page_for(@post, deletion: true)
    else
      render :edit
    end
  end

  private

  def post_params
    params.require(:post).permit(
      :category, :parent_id, :title, :content, :upload, :visible
    )
  end

  def suitable_page_for(post, deletion: false)
    case post.category
    when 'comment'
      suitable_page_for(post.parent)
    when 'download'
      deletion ? downloads_path : download_path(post)
    when 'article'
      deletion ? articles_path : article_path(post)
    else
      '/'
    end
  end
end
