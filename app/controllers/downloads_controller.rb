# frozen_string_literal: true

# This handles anything that should appear specifically under "Downloads".
class DownloadsController < ApplicationController
  def index
    @downloads = available_posts.download.order(created_at: :desc)
  end

  def show
    @download = available_posts.download.find(params[:id])
  end
end
