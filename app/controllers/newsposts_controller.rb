# frozen_string_literal: true

# This handles anything that should appear specifically under "News Posts".
class NewspostsController < ApplicationController
  def index
    current_page = [params[:page].to_i, 0].max # Enforce a minimum value of 0
    @newsposts = paginate(available_posts.newspost, current_page, 5)
  end

  private

  # NOTE: This could probably be extracted and re-used elsewhere, if necessary.
  def paginate(records, page, per_page)
    offset = page * per_page

    {
      page:,
      data: records.order(created_at: :desc).offset(offset).limit(per_page),
      earlier: records.count > offset + per_page,
      later: page.positive?
    }
  end
end
