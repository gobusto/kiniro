# frozen_string_literal: true

# This is the base controller class from which all others descend.
class ApplicationController < ActionController::Base
  def available_posts
    result = Post.all
    result = result.where(visible: true) if current_user.nil?
    result = result.where(user_id: params[:user]) unless params[:user].blank?
    result
  end
end
