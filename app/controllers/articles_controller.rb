# frozen_string_literal: true

# This handles anything that should appear specifically under "Articles".
class ArticlesController < ApplicationController
  def index
    @articles = available_posts.article.order(created_at: :desc)
  end

  def show
    @article = available_posts.article.find(params[:id])
  end
end
