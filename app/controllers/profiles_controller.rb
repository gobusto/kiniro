# frozen_string_literal: true

# Handles everything related to User CRUD.
class ProfilesController < ApplicationController
  before_action :authenticate_user!, except: %i[index show]
  before_action -> { @user = User.new }, only: %i[new create]
  before_action(
    -> { @user = User.find(params[:id]) }, except: %i[index new create]
  )

  def index
    @users = User.all
  end

  def show; end

  def new
    render :edit
  end

  def create
    if @user.update(profile_params)
      redirect_to profiles_path
    else
      render :edit
    end
  end

  def edit; end

  def update
    if @user.update(profile_params)
      redirect_to profiles_path
    else
      render :edit
    end
  end

  def destroy
    if @user.destroy
      redirect_to profiles_path
    else
      render :edit
    end
  end

  private

  def profile_params
    params.require(:user).permit(:name, :avatar, :about)
  end
end
