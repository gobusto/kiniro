# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.1.0'

# To bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '7.0.2.2'

# Use MySQL as the database for Active Record:
gem 'mysql2', '~> 0.5.2'

# Use Puma as the app server:
gem 'puma', '~> 5.6'

# Use SCSS for stylesheets:
gem 'sass-rails', '6.0.0'

# Turbolinks makes navigating your web application faster.
gem 'turbolinks', '~> 5.2.0'

# Reduces boot times through caching; required in config/boot.rb
# gem 'bootsnap', '1.4.4', require: false

# Required for generating thumbnails from ActiveStorage images:
# gem 'mini_magick', '~> 4.11'
gem 'image_processing', '~> 1.2'

# Server-side Markdown support:
gem 'redcarpet', '3.5.1'

# User account management stuff.
gem 'devise', '~> 4.8.1'

# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger
  gem 'byebug', '~> 11.1.3'
  # Use RSpec instead of the built-in Rails testing stuff:
  gem 'rspec-rails', '~> 5.1'
  # Model factories:
  gem 'factory_bot_rails', '~> 6.2'
end

group :development do
  gem 'rubocop', '1.25.1'
  # Add database column information as a comment at the top of model files:
  gem 'annotate', '~> 3.2.0'
  # https://github.com/awesome-print/awesome_print
  gem 'amazing_print', '~> 1.4'
  # Spring speeds up development by keeping your application running
  # gem 'spring', '~> 4.0'
  # gem 'spring-watcher-listen', '~> 2.0', '>= 2.0.1'
  gem 'listen', '~> 3.7', '>= 3.7.1'
end

group :test do
  # Generate code coverage reports after RSpec has run:
  gem 'simplecov', '~> 0.21.2'
end
