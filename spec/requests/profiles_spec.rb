# frozen_string_literal: true

RSpec.describe 'profiles', type: :request do
  it 'displays the list of users' do
    create(:user, name: 'Brian', about: 'I like pizza')

    get '/profiles'
    expect(response.body).to include('Brian')
    expect(response.body).to include('I like pizza')
    expect(response.status).to eq(200)
  end
end
