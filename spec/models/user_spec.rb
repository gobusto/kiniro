# frozen_string_literal: true

RSpec.describe 'User', type: :model do
  it 'is invalid if no attributes exist' do
    messages = ["Email can't be blank", "Password can't be blank"]

    user = User.new
    expect(user.valid?).to eq(false)
    expect(user.errors.full_messages).to eq(messages)
  end

  context 'to_s' do
    it 'uses the name, if one is present' do
      user = User.new(email: 'example@test.com', name: 'Hello, World!')
      expect(user.to_s).to eq('Hello, World!')
    end

    it 'falls back to the email address if no name is given' do
      user = User.new(email: 'example@test.com', name: '')
      expect(user.to_s).to eq('example@test.com')
    end
  end
end
