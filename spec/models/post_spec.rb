# frozen_string_literal: true

RSpec.describe 'Post', type: :model do
  context 'newspost' do
    it 'is invalid if no attributes exist' do
      messages = [
        'User must exist',
        "Title can't be blank",
        "Content can't be blank"
      ]

      post = Post.new(category: 'newspost')
      expect(post.valid?).to eq(false)
      expect(post.errors.full_messages).to eq(messages)
    end

    it 'cannot have an attachment' do
      upload = fixture_file_upload("#{Rails.root}/public/robots.txt")
      post = Post.new(category: 'newspost', upload:)
      expect(post.valid?).to eq(false)
      expect(post.errors.full_messages).to include('Upload must be blank')
    end
  end

  context 'article' do
    it 'is invalid if no attributes exist' do
      messages = [
        'User must exist',
        "Title can't be blank",
        "Content can't be blank"
      ]

      post = Post.new(category: 'article')
      expect(post.valid?).to eq(false)
      expect(post.errors.full_messages).to eq(messages)
    end

    it 'cannot have an attachment' do
      upload = fixture_file_upload("#{Rails.root}/public/robots.txt")
      post = Post.new(category: 'article', upload:)
      expect(post.valid?).to eq(false)
      expect(post.errors.full_messages).to include('Upload must be blank')
    end
  end

  context 'comment' do
    it 'is invalid if no attributes exist' do
      messages = [
        'User must exist',
        "Title can't be blank",
        "Content can't be blank",
        "Parent can't be blank"
      ]

      post = Post.new(category: 'comment')
      expect(post.valid?).to eq(false)
      expect(post.errors.full_messages).to eq(messages)
    end

    it 'cannot have an attachment' do
      upload = fixture_file_upload("#{Rails.root}/public/robots.txt")
      post = Post.new(category: 'comment', upload:)
      expect(post.valid?).to eq(false)
      expect(post.errors.full_messages).to include('Upload must be blank')
    end
  end

  context 'download' do
    it 'is invalid if no attributes exist' do
      messages = [
        'User must exist',
        "Title can't be blank",
        'Upload is missing'
      ]

      post = Post.new(category: 'download')
      expect(post.valid?).to eq(false)
      expect(post.errors.full_messages).to eq(messages)
    end

    it 'requires an attachment' do
      upload = fixture_file_upload("#{Rails.root}/public/robots.txt")
      post = Post.new(category: 'download', upload:)
      post.valid?
      expect(post.errors.full_messages).not_to include('Upload must be blank')
    end
  end

  it 'uses the title attribute as the to_s text' do
    expect(Post.new(title: 'Hello, World!').to_s).to eq('Hello, World!')
  end
end
