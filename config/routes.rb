# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users
  resources :profiles, except: %i[show]

  resources :newsposts, only: %i[index]
  resources :articles, only: %i[index show]
  resources :downloads, only: %i[index show]
  resources :posts, except: %i[index show]

  root 'newsposts#index'
end
