# frozen_string_literal: true

# Add an "about" column to the users table initially created by Devise.
class AddAboutToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :about, :text
  end
end
