# frozen_string_literal: true

# "Comments" are actually used for various kinds of post, so...
class RenameCommentsToPosts < ActiveRecord::Migration[5.2]
  def change
    rename_table :comments, :posts
  end
end
