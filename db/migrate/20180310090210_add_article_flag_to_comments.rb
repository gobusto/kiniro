# frozen_string_literal: true

# Allow a "comment" to be flagged as a stand-alone "article" instead.
class AddArticleFlagToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :article, :boolean, default: false, null: false
  end
end
