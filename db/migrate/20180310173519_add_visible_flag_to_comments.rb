# frozen_string_literal: true

# Allow for "draft" posts (or simply hidden ones):
class AddVisibleFlagToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :visible, :boolean, default: false, null: false
  end
end
