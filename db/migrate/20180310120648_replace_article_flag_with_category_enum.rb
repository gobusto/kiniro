# frozen_string_literal: true

# Expand on the previous migration by allowing comments to be classified:
class ReplaceArticleFlagWithCategoryEnum < ActiveRecord::Migration[5.2]
  def change
    remove_column :comments, :article, :boolean, default: false, null: false
    add_column :comments, :category, :integer, default: 0, null: false
  end
end
