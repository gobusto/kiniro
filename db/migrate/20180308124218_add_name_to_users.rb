# frozen_string_literal: true

# Add a "name" column to the users table previously created by Devise.
class AddNameToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :name, :string
  end
end
