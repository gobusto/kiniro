# frozen_string_literal: true

# Create the initial comments table.
class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.references :parent, polymorphic: true, index: true
      t.references :user, index: true
      t.string :title
      t.text :content
      t.timestamps
    end
  end
end
