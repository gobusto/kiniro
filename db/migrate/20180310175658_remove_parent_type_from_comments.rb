# frozen_string_literal: true

# The parent of a comment is always another comment...
class RemoveParentTypeFromComments < ActiveRecord::Migration[5.2]
  def change
    remove_column :comments, :parent_type, :string
  end
end
